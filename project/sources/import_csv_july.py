#!/usr/bin/env python3

import csv
import matplotlib.pyplot as plt

""" ######### Notes ######### """
"""
makelistnationality : il faudrait que ça trouve la colonne par lui-même de nationalité au lieu de devoir le spécifier

"""

""" ######### Variables declarations ######### """
liste=[] #list of all the elements from the .csv file
trainingset = []
testset = []
fullset = []
listMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']


""" ######### Functions ######### """


"""
Variables : 
    - datasetList: list of dataset
Purpose :
    Separate the datasets into three partitioning datasets
"""


def partitioning(datasetList):
    for i in range(1, 4452):
        annee = int(datasetList[i][2])
        if annee >= 2010 and annee <= 2014:
            trainingset.append(datasetList[i])
        else:
            testset.append(datasetList[i])
    fullset = trainingset + testset
    return fullset

"""
N'EST PAS UTILISEE POUR L'INSTANT
###################################################################

"""
"""
Variables : 
    - line1 : line of a list
    - line2 : line of a list
Purpose:
    returns -1 if the nationality of each line are different
    returns 0 otherwise
"""


def nationalityDifferent(line1, line2):
    if (line1 != line2):
        return -1
    else:
        return 0

"""

###################################################################

"""

"""
Variables :
    - list : list of dataset
    - nat : string describing a nationality
Purpose :
    inserts every row of the dataset into the list if the nationality of the list corresponds to nat
    returns a list
"""


def dictByCountry(list, nat):
    nationality = nat
    listCountry = []
    dct = {
        'Continent':'',
        'Nationality':'',
        'Year':[],
        'Month': [],
        'Tourists': [],
    }
    j = 0
    #print(list)
    for i in range(0, len(list)):
        if nationality in list[i]:
            j = j+1
            listCountry.append(list[i])
            dct['Continent'] = list[i][0]
            dct['Nationality'] = list[i][1]
            dct['Year'].append(list[i][2])
            dct['Month'].append(list[i][3])
            dct['Tourists'].append(list[i][4])
    #print(dct)
    return dct


"""
Variables : 
    - list : list of datasets
Purpose :
    Creates a list with all the nationality of a dataset
    Returns a list of nationality
"""


def makeNationalityList(list):
    listNationality = []
    for i in range(0, len(list)):
        if list[i][1] not in listNationality:
            listNationality.append(list[i][1])
    #print(listNationality)
    return listNationality



"""
Variables :
    - list : list of datasets (training set, validation set or test set)
Purpose :
    Returns a list of all the years entered in the dataset, not repeating the years
"""


def makeYearList(list):
    listYear = []
    for i in range(0, len(list)):
        if list[i][2] not in listYear:
            listYear.append(list[i][2])
    return listYear



"""
Variable:
    - set : list of the training set, validation ste or test set
Purpose:
    Returns a list of the name of the month associated with the years of the dataset
Example:
    set : training set
    [Jan2010, Feb2010, ..., Dec2013]
"""


def createCalender(set):
    listCalendar = []
    for i in makeYearList(set):
        for j in listMonth:
            listCalendar.append(j+i)
    return listCalendar



"""
Variables :
    - list : list of datasets
Purpose:
    Creates a dictionary with the data separated for each country
    returns a dictionary
"""


def createDictionaryCountry(list):
    dct = {}
    n = makeNationalityList(list)
    l = []
    for i in n:
        l.append(dictByCountry(list, i))
    #print(l)
    return l


def getYearMonthPosition(year, month, dict):
    listYear = dict.get('Year')
    pos=0
    month=int(month)
    #print(listYear)
    #listMonth = dict.get("Month")
    try:
        positionYear = listYear.index(str(year))
        #print(positionYear)
    except ValueError:
        print('This year is not in the list')
    #listYear = listYear[positionYear:12*(positionYear+1)]
    listMonth = dict.get('Month')
    pos=month+12*positionYear
    #print(pos)
    return pos

"""
Variables :
    - set : the training set, validation set or test set
    - country : string, the nationality
    - year : string, the year needed
Purpose :
    returns a list of the number of tourists for a given year and nationality
"""


def listTouristCountryMonthByMonth(set, country, year):
    listTouristYear = [] #list of number of tourists of one given year
    d = dictByCountry(set, country)
    print(d)
    listDictionaryTourist = d['Tourists'] #list of number of tourists given by the dictionnary for multiple years
    listYear = d['Year']
    positionYear = listYear.index(str(year))
    print(positionYear)
    for i in range (positionYear, positionYear+12):
        listTouristYear.append(listDictionaryTourist[i])
    print(listTouristYear)
    return listTouristYear


"""
Variables :
    - set : list of the training set, validation set or test set
    - country : string og the nationality wanted
    - month : int of the month wanted
Purpose :
    Returns a list of the number of tourists for one wanted month of multiple years
Example :
    Set used : training set
    Month wanted : 1
    [1,2,3] : 1 tourist in Jan2010, 2 tourists in Jan2011, 3 tourists in Jan2012
"""


def nbTouristOneMonthMultYears(set, country, month):
    d = dictByCountry(set, country)
    listTouristMonth = []
    listDictionaryTourist = d['Tourists']
    year = makeYearList(set)
    for i in year:
        listTouristMonth.append(int(listDictionaryTourist[month]))
        month = month + 12
    return listTouristMonth


"""
Variables : 
    - set : training set, validation set or test set
    - country : string of nationality wanted
    - month : int of month wanted
Purpose :
    Creates a scatter plot displaying the evolution of the number of tourists of one month depending on years
"""


def scatterChartTouristByMonth(set, country, month):
    fig = plt.figure()
    height = []
    plt.title("Month %s in %s" %(listMonth[month], country))
    listTourist = nbTouristOneMonthMultYears(set, country, month)
    for i in listTourist:
        height.append(int(i))
    x = makeYearList(set)
    width = 1.0
    plt.plot(x, height)
    plt.savefig('scatterChart.png')
    plt.show()


"""
Variables : 
    - set : training set, validation set or test set
    - country : string of nationality wanted
    - year : int of month wanted
Purpose :
    Creates a scatter plot displaying the evolution of the number of tourists of one month depending on years
"""


def scatterChartTouristByYear(set, country, year):
    fig = plt.figure()
    plt.title("Year %d in %s" %(year, country))
    height = []
    l = listTouristCountryMonthByMonth(set, country, year)
    for i in l:
        height.append(int(i))
    x = listMonth
    plt.plot(x, height)
    plt.savefig('scatterchartTouristByYear.png')
    plt.show()



"""
Variables:
    - set : list of the training set, validation set or test set
    - country : string of the nationality wanted
Purpose :
    Creates a plot with the evolution of the number of tourists coming from the country depending on time
"""


def plotChartTouristNat(set, country):
    d = dictByCountry(set, country)
    l = d['Tourists']
    height = []
    for i in l:
        height.append(int(i))
    x = createCalender(set)

    fig = plt.figure()
    axes = plt.gca()
    plt.plot(x, height, 'o', ls='-', ms=3, markevery=1)
    plt.xticks(rotation='vertical')
    plt.tick_params(axis='x', labelsize=7)
    plt.title(country)
    plt.savefig('plotChartTouristNat.png')
    plt.show()



"""
Variables :
    - fullset : the whole set, including the training set and the test set
    - country : string of the nationality wanted
Purpose :
    Draws boxplots for the years of the set depending on the month. The number of tourists is evaluated.
Example :
    Boxplot of January indicates the mean/the median/the quartiles of the number of tourists of Jan10, Jan11, Jan2012, etc. all the January
"""


def boxplotMonthByMonth(fullset, country):
    d = dictByCountry(fullset, country)
    x = listMonth
    listBoxPlot = []
    for i in range(0,12):
        l = nbTouristOneMonthMultYears(fullset, country, i)
        listBoxPlot.append(l)

    fig = plt.figure()
    plt.title(country)
    medianprops = {'color':'black'}
    meanprops = {'marker':'o', 'markeredgecolor':'black', 'markerfacecolor':'firebrick'}
    plt.boxplot(listBoxPlot, showfliers=False, medianprops=medianprops, vert=False, patch_artist=True, showmeans=True, meanprops=meanprops)
    plt.savefig('BoxPlotMonthByMonth.png')
    plt.show()



"""
Variables : 
    - set : list of the training data or test data
    - country : string of the nationality
Purpose :
    returns a list of a list of the number of tourists for one given month over the years
Example :
    [[Nb tourists Jan2010, Nb tourists Jan2011, Nb tourists Jan2012], [Nb tourists Feb2010, ...] ...]
"""


def listMonthByMonthYears(set, country):
    listTotal = []
    for i in range(0,12):
        l = nbTouristOneMonthMultYears(set, country, i)
        listTotal.append(l)
    return listTotal


"""
Variables :
    - set : list of the training set or test set
    - country : string of the nationality
Purpose :
    Returns a number indicating the average difference of the number of tourists for a given country 
    if we use the average mathematical definition
"""
def functionAverage(set, country):
    l = listMonthByMonthYears(set, country)
    meanTheo = []
    lastElement = []
    diff = []
    sumDiff = 0
    for i in range(0, 12):
        s=0
        for j in range(0, len(l[i])):
            if (j<len(l[i])-1):
                s = s+l[i][j]
            else:
                lastElement.append(l[i][j])
        meanTheo.append(s/4)
    for i in range(0,11):
        diff.append(lastElement[i] - meanTheo[i])
    for i in diff:
        sumDiff = sumDiff+i
    print(sumDiff/len(diff))
    return sumDiff/len(diff)











""" ######### Main program ######## """


def main() :
    with open('csv_file/thaitourism2.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            liste.append(row)
    f = partitioning(liste)
    l2 = createDictionaryCountry(trainingset)
    #print(getYearMonthPosition('2012','2',dictByCountry(trainingset,'SoAfrica')))
    #plt.plot([1, 2, 3, 4])
    #plt.ylabel('Label 1')
    #plt.show()
    #plotCountryMonthByMonth(trainingset, 'SoAfrica', 2010)

    #scatterChartTouristByMonth(trainingset, 'Vietnam', 6)
    #scatterChartTouristByYear(trainingset, 'Vietnam', 2011)
    plotChartTouristNat(trainingset, 'France')
    #boxplotMonthByMonth(f, 'Vietnam')

    #print(nbTouristOneMonthMultYears(trainingset, 'France', 0))
    functionAverage(trainingset, 'Vietnam')



if __name__ == '__main__':
    main()