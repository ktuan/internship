"""print("Hello world !")
i = input()
try:
    i = int(i)
except:
    print("Ce n'est pas un entier\n")
while i<5:
    if i<3:
        print("i est inferieur a 3")
    else:
        print("i superieur a 3")
    i+=1
"""

import random as random2


# Somme misée par le joueur
def somme(s):
    if s < 0:
        return -1
    else:
        return 0

# case choisie pour la mise
def mise():
    m = input()
    try:
        m = int(m)
    except TypeError:
        print("Le chiffre doit être un entier \n")

    if m < 0:
        return -1
    elif m > 59:
        return -1
    else:
        print("Le chiffre choisi pour la mise est %d \n",m)
        return m

# Renvoie la couleur de la case
def couleur(case):
    if case % 2 == 0:
        return "NOIRE"
    else:
        return "ROUGE"

#renvoie le multiple de son gain selon sa mise et l'événement
def gagne(c1, c2):
    if c1 == c2:
        return 3
    elif couleur(c1) == couleur(c2):
        return 0.5
    else:
        return 0

def jeu():
    sommeJoueur = input()
    if somme(sommeJoueur) == 0:
        miseJoueur = mise()
        caseAleatoire = random2.randrange(50)
        print("La case tombée : %d", caseAleatoire)
        gain = gagne(miseJoueur,caseAleatoire)
        if gain==0:
            sommeJoueur=0
        else:
            sommeJoueur = sommeJoueur + sommeJoueur*gain
        print("Votre nouveau solde est : %d", sommeJoueur)

jeu()